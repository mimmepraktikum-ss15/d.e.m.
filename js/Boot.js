// Boot.js is the state where a possible loading screen is defined

var dem = dem || {};

dem.Boot = function(){};

dem.Boot.prototype = {
  // this is where the assets for a loading screen get loaded. 
  preload: function(){
    // assets to load for the loading screen
    this.load.image('preloadbar', 'assets/images/preloader-bar.png');
    this.load.image('heroload', 'assets/images/hero1big.png');
    this.load.image('dino2load', 'assets/images/dino2big.png');
  },

  create: function(){
    // create background for loading screen here
    this.game.stage.backgroundColor = '#000';

    // this makes phaser show all of a canvas of the size defined in main.js without scaling
    this.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
    // this aligns the game in the center of the screen
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;

    // set physics
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    // boot.js starts preload state
    this.state.start('Preload');
  }
};