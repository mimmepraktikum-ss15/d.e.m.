// Help.js is where the player can see how to play

var dem = dem || {};

dem.Help = function(){};

dem.Help.prototype ={
	preload: function(){

	},

	create: function(){
		// fontstyles for title and normal text
		var titleStyle = {font: "50px Arial", fill: "#fff", align: "center" };
		var style = {font: "32px Arial", fill: "#fff", align: "center" };

		// adds title text
	    this.helpText = this.add.text(640/2-140, 10, 'How to play:', titleStyle);
	    
	    // adds control images and additional text hints
	    this.arrowUpImg = this.game.add.sprite (40, 100, 'up');
	    this.arrowUpText = this.add.text(82, 100, ' = Jump', style);
	    
	    this.arrowLeftImg = this.game.add.sprite(40, 147, 'left');
	 	this.arrowLeftText = this.add.text(82, 147, ' = Move left', style);

	    this.arrowRightImg = this.game.add.sprite(40, 194, 'right');
	    this.arrowRightText = this.add.text(82, 194, ' = Move right', style);
	    
	    this.escImage = this.game.add.sprite (40, 241, 'esc');
	    this.escText = this.add.text(82, 231, ' = Quit game', style);

	    this.spaceImg = this.game.add.sprite (330, 100, 'space');
	    this.spaceText = this.add.text(436, 100, ' = Shoot', style);

	    this.pauseImg = this.game.add.sprite (330, 147, 'pauseButton');
	    this.pauseText = this.add.text(372, 147, ' = Pause game', style);

	    this.muteImg = this.game.add.sprite (330, 194, 'muteButton');
	    this.muteText = this.add.text(372, 194, ' = Mute sound', style);

	    // adds the back button
	    this.backButton = this.game.add.button (566, 278, 'back', this.backPressed, this);
	},

	// if back button is pressed, the title state is started
	backPressed: function(){
		this.state.start ('Title');
	},
}