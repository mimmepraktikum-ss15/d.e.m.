// Level.js is where the player can select a specific level to play 

var dem = dem || {};

dem.Level = function(){};

dem.Level.prototype ={

	// gets the menu music
	init: function(music){
		this.menuMusic = music;
	},

	preload: function(){
	 
	},

	create: function(){
		// resets the level variable
		this.level = 1;

		// sets style for title and other text
		var titleStyle = {font: "50px Arial", fill: "#fff", align: "center" };
		var style = {font: "30px Arial", fill: "#fff", align: "center" };

		// adds title text
	    this.levelText = this.add.text(640/2 - 170, 20, 'Select a Level: ', titleStyle);
	    
	    // adds a preview image of the level as button and an additional text
	    this.lvl1button = this.game.add.button (88, 110, 'level1', this.lvl1selected, this);
	    this.lvl1text = this.game.add.text (88, 220, 'Level 1', style);

	    this.lvl2button = this.game.add.button (272, 110, 'level2', this.lvl2selected, this);
	    this.lvl2text = this.game.add.text (272, 220, 'Level 2', style);

	    this.lvl3button = this.game.add.button (456, 110, 'level3', this.lvl3selected, this);
	    this.lvl3text = this.game.add.text (456, 220, 'Level 3', style);

	    // adds the back button
	    this.backButton = this.game.add.button (566, 278, 'back', this.backPressed, this);
	},

	// if back button is pressed, the title state is started
	backPressed: function(){
		this.state.start ('Title');
	},

	// stops menu music and starts level 1
	lvl1selected: function(){
		this.menuMusic.stop();
		this.level = 1;
		this.state.start ('Game', true, false, this.level);
	},

	// stops menu music and starts level 2
	lvl2selected: function(){
		this.menuMusic.stop();
		this.level = 2;
		this.state.start ('Game', true, false, this.level);
	},

	// stops menu music and starts level 3
	lvl3selected: function(){
		this.menuMusic.stop();
		this.level = 3;
		this.state.start ('Game', true, false, this.level);
	},
}