// Over.js is the screen that shows up when the player dies 

var dem = dem || {};

dem.Over = function(){};
// fontstyle for title text
this.titleStyle = {font: "50px Arial", fill: "#fff", align: "center" };

dem.Over.prototype ={

	init: function(level){
		// sets level variable
		this.replayLevel = level;
	},

	preload: function(){
	 
	},

	create: function(){
		// adds title text
	    this.titleText = this.add.text(640/2-130, 10, 'Game Over', titleStyle);
	    // adds restart button
		this.restartButton = this.game.add.button((640-64)/2, 278, 'again', this.restartPressed, this);
		// adds the back button
		this.backButton = this.game.add.button (566, 278, 'back', this.backPressed, this);
		// adds game over sprite
		this.grave = this.add.sprite ((640-128)/2, (320-128)/2 + 20, 'gameOver');
	},

	// if back button is pressed, title state gets started
	backPressed: function(){
		this.state.start ('Title');
	},

	// if restart is pressed, the level played before gets started again
	restartPressed: function(){
		this.state.start ('Game', true, false, this.replayLevel);
	},
}