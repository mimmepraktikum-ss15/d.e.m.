// this is the start of the project
// main.js is included in index.html so it starts with loading the index.html file.

// main.js creates the namespace for the game
var dem = dem || {};

// main.js creates a new phaser game
// canvas will show with 640 px width and 320 px hight
// Phaser.AUTO is for rendering and tries WebGL or Canvas depending on browser compatibility
// the empty string at the end is for appending the game to a DOM object of choice. 
// an empty string means the game is simply appended to the body
dem.game = new Phaser.Game(640, 320, Phaser.AUTO, '');

// main.js creates the states for the game

// Boot.js is the state where a loading screen is defined
dem.game.state.add('Boot', dem.Boot);
// Preload.js is the state where the assets and the game world is loaded
dem.game.state.add('Preload', dem.Preload);
// Title.js is the state where the player can press the start button
dem.game.state.add('Title', dem.Title);
// Game.js is the state where the magic happens
dem.game.state.add('Game', dem.Game);
// Over.js is the state where the player can restart the game after a death
dem.game.state.add('Over', dem.Over);
// Won.js is the state where the player can see his score, restart the level or play the next one.
dem.game.state.add('Won', dem.Won);
// Level.js is the state where the player can choose a specific level
dem.game.state.add('Level', dem.Level);
// Score.js is the state where the player can see his score
dem.game.state.add('Score', dem.Score);
// Help.js is the state where the player can see the controls
dem.game.state.add('Help', dem.Help);



// main.js starts the boot state
dem.game.state.start('Boot');