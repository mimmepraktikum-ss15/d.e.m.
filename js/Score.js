// Score.js is where the player can see his highscores

var dem = dem || {};

dem.Score = function(){};

dem.Score.prototype ={
	preload: function(){

	},

	create: function(){
		// score variables
		this.score1 = 0;
		this.score2 = 0;
		this.score3 = 0;

		// checks if localStorage is avaible and if a level score is already scored
		// if so it loads the score
		if (this.game.device.localStorage) {
			if (localStorage.level1){
				this.score1 = localStorage.level1;
			}
			if (localStorage.level2){
				this.score2 = localStorage.level2;
			}
			if (localStorage.level3){
				this.score3 = localStorage.level3;
			}	  		
		}

		// fontstyles for title and normal text
		var titleStyle = {font: "50px Arial", fill: "#fff", align: "center" };
		var style = {font: "30px Arial", fill: "#fff", align: "center" };

		// adds title text
	    this.titleText = this.add.text(640/2-130, 10, 'Your Score:', titleStyle);

	    // adds score text
	    this.score1Text = this.add.text (640/2 - 80, 120, 'Level 1: ' + this.score1, style);
	    this.score2Text = this.add.text (640/2 - 80, 170, 'Level 2: ' + this.score2, style);
	    this.score3Text = this.add.text (640/2 - 80, 220, 'Level 3: ' + this.score3, style);
		
		// adds the back button    
	    this.backButton = this.game.add.button (566, 278, 'back', this.backPressed, this);
	},

	// if back button is pressed, the title state is started
	backPressed: function(){
	this.state.start ('Title');
	},
}