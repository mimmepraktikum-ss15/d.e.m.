// Won.js is the screen that shows up after a level is completed 

var dem = dem || {};

dem.Won = function(){
	// fontstyles for title text
	this.titleStyle = {font: "50px Arial", fill: "#fff", align: "center" };
	this.scoreStyle = {font: "14px Arial", fill: "#fff", align: "center" };
	this.finalStyle = {font: "24px Arial", fill: "#fff", align: "center" };
};

dem.Won.prototype ={

	// Won gets the level, the score, the number of kills, the number of collected coins and the time left
	init: function(level, score, kills, coins, time){
		this.replayLevel = level;
		this.playerScore = score;
		this.killScore = kills;
		this.coinScore = coins;
		this.timeLeft = time;

		// calculates the final score
		this.finalScore = this.killScore*5 + this.coinScore*3 +this.timeLeft*1;
	},

	preload: function(){
	 
	},

	create: function(){
		// adds title text depending on level
	    this.titleText = this.add.text((640-240)/2 -60, 10, 'Level ' + this.replayLevel + ' complete!', this.titleStyle);
	    
	    // adds text for kills, coins time and score
	    this.killsText = this.add.text((640/2)-105, 80, '   Enemies killed: ' + this.killScore + '   x5', this.scoreStyle);
	    this.coinsText = this.add.text((640/2)-105, 100, '+ Coins collected: ' + this.coinScore + '   x3', this.scoreStyle);
	    this.timeText = this.add.text((640/2)-105, 130, '+ Time left: ' + this.timeLeft + ' seconds', this.scoreStyle);
	    this.finalText = this.add.text((640/2)-105, 170, '= Total score: ' + this.finalScore, this.finalStyle);
		
		// adds invisible highscore text
		this.highscoreText = this.add.text((640/2-250), 210, 'NEW HIGHSCORE !!!', this.titleStyle);
		this.highscoreText.visible = false;

		// checks the high score
		this.checkHighscore();

		// adds buttons to restart level or play the next one
		this.restartButton = this.game.add.button(320-64-5, 320 - 32 - 10, 'again', this.restartPressed, this);
		this.nextButton = this.game.add.button (320+5, 320-32-10, 'next', this.nextPressed, this);
		
		// adds the back button
		this.backButton = this.game.add.button (566, 278, 'back', this.backPressed, this);
	},

	// this checks if the score is a highscore
	checkHighscore: function(){
		// checks depending on level
		switch (this.replayLevel) {
            case 1:
            	// checks if a score is in localStorage
                if (localStorage.level1){
                   	// checks if score is higher than stored score
                   	if (this.finalScore > localStorage.level1){
                   		// if higher it stores the score and sets the highscore text visible
                   		localStorage.level1 = this.finalScore;
                   		this.highscoreText.visible = true;
                   	}
                } else {
                	// if no score is available, score is highscore
                	localStorage.level1 = this.finalScore;
                   	this.highscoreText.visible = true;
                }
                break;

            case 2:
                if (localStorage.level2){
                   	if (this.finalScore > localStorage.level2){
                   		localStorage.level2 = this.finalScore;
                   		this.highscoreText.visible = true;
                   	}
                } else {
                	localStorage.level2 = this.finalScore;
                   	this.highscoreText.visible = true;
                }
                break;

            case 3:
                if (localStorage.level3){
                   	if (this.finalScore > localStorage.level1){
                   		localStorage.level3 = this.finalScore;
                   		this.highscoreText.visible = true;
                   		console.log('case3');
                   	}
                } else {
                	localStorage.level3 = this.finalScore;
                   	this.highscoreText.visible = true;
                }
                break;
                
            // there is no default action here   
            default:
        }
	},

	// if back button is pressed, title state gets started
	backPressed: function(){
		this.state.start ('Title');
	},

	// if restart is pressed, the same level as before gets started
	restartPressed: function(){
		this.state.start ('Game', true, false, this.replayLevel);
	},

	// if next is pressed, the next level gets started
	// if actual level is level 3, next button starts level 3 again
	nextPressed: function (){
		if (this.replayLevel < 3){
			this.replayLevel++;
			this.state.start ('Game', true, false, this.replayLevel);
		}else{
			this.restartPressed();
		}
	}, 
}