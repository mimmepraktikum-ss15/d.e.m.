// Preload.js is the state where the assets and the game world are loaded

var dem = dem || {};

dem.Preload = function(){};

dem.Preload.prototype = {

    preload: function(){
        // this is where the look and the functionality of the loading screen is created
        
        // font style for text
        var style = {font: "50px Arial", fill: "#fff", align: "center" };

        // loading text
        this.loadtext = this.add.text(640/2-110, (320-130)/2 + 30, 'loading...', style);

        // preloadbar and additional graphics
        this.preloadBar = this.add.sprite ((640-350)/2, (320-35)/2+95, 'preloadbar');
        this.heroload = this.add.sprite (21, 320-128-10, 'heroload');
        this.dino1load = this.add.sprite (640-128-10, 320-128-10, 'dino2load');
        this.load.setPreloadSprite(this.preloadBar);


        // all game assets will be loaded here with a 'key' and a 'source/path/file' and additional parameters if needed
        
        // preloading the tilemaps
        this.load.tilemap('lvl1', 'assets/tilemaps/lvl1.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('lvl2', 'assets/tilemaps/lvl2.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('lvl3', 'assets/tilemaps/lvl3.json', null, Phaser.Tilemap.TILED_JSON);

        
        // preloading images
        // logo
        this.load.image('logo', 'assets/images/demlogo.png');

        // tiles
        this.load.image('gametiles', 'assets/images/tiles.png');
        this.load.image('gametiles2', 'assets/images/tiles2.png');
        this.load.image('gametiles3', 'assets/images/tiles3.png');
        this.load.image('gametiles4', 'assets/images/tiles4.png');
        this.load.image('gametiles5', 'assets/images/tiles5.png');

        // level previews for level selection
        this.load.image('level1', 'assets/images/lvl1.png');
        this.load.image('level2', 'assets/images/lvl2.png');
        this.load.image('level3', 'assets/images/lvl3.png');

        // menu buttons 
        this.load.image('play', 'assets/images/play.png');
        this.load.image('score', 'assets/images/score.png');
        this.load.image('level', 'assets/images/level.png');
        this.load.image('help', 'assets/images/help.png');
        this.load.image('again', 'assets/images/again.png');
        this.load.image('next', 'assets/images/next.png');
        this.load.image('back', 'assets/images/back.png');

        // game sprites
        this.load.image('dino1', 'assets/images/dino1.png');
        this.load.image('dino2', 'assets/images/dino2.png');
        this.load.image('dino3', 'assets/images/dino3.png');
        this.load.image('bulletRight', 'assets/images/bulletRight.png');
        this.load.image('bulletLeft', 'assets/images/bulletLeft.png');
        this.load.image('hero1', 'assets/images/hero1.png');
        this.load.image('bones', 'assets/images/bones.png');
        this.load.image('goal', 'assets/images/goal.png');
        
        // controls images for help screen
        this.load.image('up', 'assets/images/up.png');
        this.load.image('left', 'assets/images/left.png');
        this.load.image('right', 'assets/images/right.png');
        this.load.image('space', 'assets/images/space.png');
        this.load.image('esc', 'assets/images/esc.png');
        this.load.image('pauseButton', 'assets/images/pause.png');
        
        // image for game over screen
        this.load.image('gameOver', 'assets/images/gameOver.png');

        // image for title screen
        this.load.image('dinoDead', 'assets/images/dinoDead.png');

        // preloading animation spritesheets
        this.load.spritesheet('muteButton', 'assets/images/mute.png', 32, 32, 2);
        this.load.spritesheet('herowalk', 'assets/images/herowalk.png', 34, 32, 4);
        this.load.spritesheet('dinowalk', 'assets/images/dinowalk.png', 66, 64, 4);
        this.load.spritesheet('dinojump', 'assets/images/dinojump.png', 34, 32, 4);
        this.load.spritesheet('dinofly', 'assets/images/dinofly.png', 66, 64, 4);
        this.load.spritesheet('dinospeed', 'assets/images/dinospeed.png', 32, 32, 4);
        this.load.spritesheet('coinflip', 'assets/images/coinflip.png', 32, 32, 8);
        this.load.spritesheet('dinoswim', 'assets/images/dinoswim.png', 64, 64, 4);
        
        // preloading sounds
        this.load.audio('shoot', 'assets/sounds/shoot.mp3');
        this.load.audio('jump', 'assets/sounds/jump.mp3');
        this.load.audio('dead', 'assets/sounds/dead.mp3');
        this.load.audio('coin', 'assets/sounds/coin.mp3');

        // preloading music
        this.load.audio('music1', 'assets/sounds/music1.mp3');
        this.load.audio('music0', 'assets/sounds/music0.mp3');
        this.load.audio('music3', 'assets/sounds/music3.mp3');
        this.load.audio('music2', 'assets/sounds/music2.mp3');
    },

    create: function(){
        // the title screen state is started here
        this.state.start('Title');
    },
};