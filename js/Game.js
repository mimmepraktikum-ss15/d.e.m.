// Game.js is where the magic happens 

var dem = dem || {};

dem.Game = function(){
    // adds variables
    this.timer;
    this.bulletArray = [];
    this.shotTimer = 0;
    this.turnTimer = 0;
    this.direction = "right";
    this.newDirection = "right";
    this.timeStyle = { font: "20px Arial", fill: "#fff", align: "center" };
    this.killsStyle = { font: "20px Arial", fill: "#fff", align: "center", stroke: "#fff", strokeThickness: 0};
    this.coinsStyle = { font: "20px Arial", fill: "#fff", align: "center", stroke: "#fff" };
    this.scoreStyle = { font: "20px Arial", fill: "#fff", align: "center", stroke: "#fff" };
    this.pausedStyle = { font: "20px Arial", fill: "#fff", align: "center", stroke: "#fff" };
    this.labelText = "0";
};

dem.Game.prototype = {
    // game gets the level
    init: function(level) {
        this.level = level || 1;
    },

    create: function() {
        // rounds the pixels, can help against collision problems
        this.game.renderer.renderSession.roundPixels = true;

        // this adds the tilemap and the music 
        this.addLevelandMusic();
        
        // this adds the tileset
        // the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        this.map.addTilesetImage('tiles', 'gametiles');
        this.map.addTilesetImage('tiles2', 'gametiles2');
        this.map.addTilesetImage('tiles3', 'gametiles3');
        this.map.addTilesetImage('tiles4', 'gametiles4');
        this.map.addTilesetImage('tiles5', 'gametiles5');

        // this creates the foreground layer specified in the tileset
        this.foregroundLayer = this.map.createLayer('foreground');
        // this creates the background layer specified in the tileset
        this.backgroundlayer = this.map.createLayer('background');
        // this creates the blocked layer specified in the tileset
        this.blockedLayer = this.map.createLayer('blocked');
        // this creates the turning layer specified in the tileset and makes it invisible
        this.turningLayer = this.map.createLayer('turning');
        this.turningLayer.alpha = 0;
 
        // this sets the collision on the blocked layer
        // the first and second parameter specify the first and last index of the tile set for collision
        // the third parameter enables collision
        // the fourth parameter is the layer the collision should work on
        this.map.setCollisionBetween(1, 1000, true, 'blocked');
        this.map.setCollisionBetween(1, 1000, true, 'turning');

        // sets the world size to match the size of this layer
        this.backgroundlayer.resizeWorld();
        
        // this creates the player from the player object in the tilemap
        var result = this.findObjectsByType('player', this.map, 'objects');
        // the sprite for the player is added in position
        this.player = this.game.add.sprite(result[0].x, result[0].y, 'herowalk');
        // sets the anchor of the spite
        this.player.anchor.setTo(0.5, 0.5);
        // adds walking animation
        this.playerWalkRight = this.player.animations.add('herowalk');

        // adds physics to the player
        this.game.physics.arcade.enable(this.player);

        // sets the camera to follow the player in the world
        this.game.camera.follow(this.player);

        // sets the player gravity
        this.player.body.gravity.y = 440;

        // lets the player collide with bounds of the world, needed for death check
        this.player.body.collideWorldBounds = true;

        // activates the cursor keys so the player can later move by pressing them
        this.cursors = this.game.input.keyboard.createCursorKeys();

        // activates the spacebar so the player can later shoot by pressing it
        this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        // activates the ESC key so the player can later pause the game by pressing it
        this.esc = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        
        // creates coins
        this.createCoins();
        // creates the level goal
        this.createGoal();
        // creates all types of enemies
        this.createEnemies();
        this.createSpeedyEnemies();
        this.createJumpingEnemies();
        this.createFlyingEnemies();
        this.createFishyEnemies();

        // this creates a bullet group
        this.bullets = this.game.add.group();
        // adds physics to group
        this.game.physics.enable(this.bullets, Phaser.Physics.ARCADE);

        // this creates the sounds
        this.weaponSound = this.game.add.audio('shoot');
        this.jumpingSound = this.game.add.audio('jump');
        this.deadSound = this.game.add.audio('dead');
        this.coinSound = this.game.add.audio('coin');

        // starts the level music
        this.music.play();

        // adds mute button and fixes it to camera
        this.muteButton = this.game.add.button((640-42), 10, 'muteButton', this.mutePressed, this);
        this.muteButton.fixedToCamera=true;
        
        // adds pause button and fixes it to camera
        this.pauseButton = this.game.add.button((640-84), 10, 'pauseButton', this.pausePressed, this);
        this.pauseButton.fixedToCamera=true;    

        // adds particle emitter for enemy death animation
        this.emitter = this.game.add.emitter(0, 0, 100);
        this.emitter.makeParticles('bones');
        this.emitter.gravity = 200;

        // brings the foregroundLayer to top, so all other elements are shown behind it
        this.foregroundLayer.bringToTop();

        // sets values for score, coins and kills
        this.score = 0;
        this.coinsCollected = 0;
        this.killCount = 0;

        // creats a countdown timer
        this.createCountdown();

        // shows all ui text labels
        this.showLabels();
    },

    // adds level tilemap and music depending on level
    addLevelandMusic: function(){
        switch (this.level) {
            case 1:
                this.map = this.game.add.tilemap('lvl1');
                this.music = this.game.add.audio('music1', 0.5, true);
            break;

            case 2:
                this.map = this.game.add.tilemap('lvl2');
                this.music = this.game.add.audio('music2', 0.5, true);
            break;

            case 3:
                this.map = this.game.add.tilemap('lvl3');
                this.music = this.game.add.audio('music3', 0.5, true);
            break;
                    
            default:
                this.map = this.game.add.tilemap('lvl1');
                this.music = this.game.add.audio('music1', 0.5, true);
        }
    },

    // shows all ui textlabels
    showLabels: function() {
        // this sets up all labels and fixes them to camera
        this.coinsLabel = this.game.add.text(160, 11, 'Coins: ' + this.labelText, this.coinsStyle);
        this.coinsLabel.fixedToCamera = true;
        this.coinsLabel.setShadow(1,1);
        this.killsLabel = this.game.add.text(290, 11, 'Kills: ' + this.labelText, this.killsStyle);
        this.killsLabel.fixedToCamera = true;
        this.killsLabel.setShadow(1,1);
        this.scoreLabel = this.game.add.text(400, 11, 'Score: ' + this.labelText, this.scoreStyle);
        this.scoreLabel.fixedToCamera = true;
        this.scoreLabel.setShadow(1,1);
        // paused text is only visible when game is paused
        this.pausedText = this.game.add.text(200, 150, "Game paused.\nClick anywhere to continue.", this.pausedStyle);
        this.pausedText.fixedToCamera= true;
        this.pausedText.visible = false;
        this.pausedText.setShadow(1,1);
    },

    // creates a countdown timer
    createCountdown: function() {
        this.timer = this.game.time.create();
        
        // sets game over as a delayed event 3m and 30s from now
        this.timerEvent = this.timer.add(Phaser.Timer.MINUTE * 3 + Phaser.Timer.SECOND * 30, this.gameOver, this);
            
        // starts the timer
        this.timer.start();
    },

    // mutes/unmutes sound and music if button is pressed
    mutePressed: function(){
        if (!this.game.sound.mute){
            this.game.sound.mute = true;
            this.muteButton.frame = 1;
        }else{
            this.muteButton.frame = 0;
            this.game.sound.mute = false;
        }
    },

    
    // lets the player jump and plays the jumping sound
    playerJump: function() {
        // only possible if player is standing on floor    
        if(this.player.body.onFloor()) {
            this.player.body.velocity.y -= 350;
            this.jumpingSound.play();    
        }   
    },

    //finds objects in a tiled layer that contains a property called "type" equal to a certain value
    findObjectsByType: function(type, map, layer) {
        var result = new Array();
        map.objects[layer].forEach(function(element){
            if(element.properties.type === type) {
                //phaser uses top left, tiled bottom left so the y position needs adjustment
                element.y -= map.tileHeight;
                result.push(element);
            }      
        });
        return result;
    },

    // creates enemies
    createEnemies: function() {
        this.enemies = this.game.add.group();
        this.enemies.enableBody = true;
        result = this.findObjectsByType('enemy', this.map, 'objects');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.enemies);
        }, this);
        // sets enemy properties
        this.enemies.setAll('body.allowGravity', true);
        this.enemies.setAll('body.immovable', true);
        this.enemies.setAll('body.gravity.y', 400);
        this.enemies.setAll('anchor.x', 0.5);
        this.enemies.setAll('anchor.y', 0.5);
        this.enemies.setAll('body.velocity.x', -30);
        this.enemies.callAll('sprite.add', 'dinowalk');
        this.enemies.callAll('animations.add', 'animations', 'dinowalk',[0,1,2,3], 3, true);  
        this.enemies.callAll('play', null, 'dinowalk');
    },
    
    //create speedyEnemies
    createSpeedyEnemies: function() {
        this.speedyEnemies = this.game.add.group();
        this.speedyEnemies.enableBody = true;
        result = this.findObjectsByType('speedyEnemy', this.map, 'objects');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.speedyEnemies);
        }, this);
        // sets enemy properties
        this.speedyEnemies.setAll('body.allowGravity', true);
        this.speedyEnemies.setAll('body.immovable', true);
        this.speedyEnemies.setAll('body.gravity.y', 400);
        this.speedyEnemies.setAll('anchor.x', 0.5);
        this.speedyEnemies.setAll('anchor.y', 0.5);
        this.speedyEnemies.setAll('body.velocity.x', -250);
        this.speedyEnemies.callAll('sprite.add', 'dinospeed');
        this.speedyEnemies.callAll('animations.add', 'animations', 'dinospeed',[0,1,2,3], 3, true);  
        this.speedyEnemies.callAll('play', null, 'dinospeed');
    },

    // creates jumping enemies
    createJumpingEnemies: function() {
        this.jumpingEnemies = this.game.add.group();
        this.jumpingEnemies.enableBody = true;
        result = this.findObjectsByType('jumpingEnemy', this.map, 'objects');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.jumpingEnemies);
        }, this);
        // sets enemy properties
        this.jumpingEnemies.setAll('body.allowGravity', true);
        this.jumpingEnemies.setAll('body.immovable', true);
        this.jumpingEnemies.setAll('body.gravity.y', 400);
        this.jumpingEnemies.setAll('anchor.x', 0.5);
        this.jumpingEnemies.setAll('anchor.y', 0.5);
        this.jumpingEnemies.setAll('body.velocity.y', -350);
        this.jumpingEnemies.callAll('sprite.add', 'dinojump');
        this.jumpingEnemies.callAll('animations.add', 'animations', 'dinojump',[0,1,2,3], 8,true);
        this.jumpingEnemies.callAll('play', null, 'dinojump');
    },

    // creats fishy enemies
    createFishyEnemies: function() {
        this.fishyEnemies = this.game.add.group();
        this.fishyEnemies.enableBody = true;
        result = this.findObjectsByType('fishyEnemy', this.map, 'objects');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.fishyEnemies);
        }, this);
        // sets enemy properties
        this.fishyEnemies.setAll('body.allowGravity', true);
        this.fishyEnemies.setAll('body.immovable', true);
        this.fishyEnemies.setAll('body.gravity.y', 400);
        this.fishyEnemies.setAll('body.collideWorldBounds', true);
        this.fishyEnemies.setAll('body.bounce.x', 1);
        this.fishyEnemies.setAll('body.bounce.y', 1);
        this.fishyEnemies.setAll('anchor.x', 0.5);
        this.fishyEnemies.setAll('anchor.y', 0.5);
        this.fishyEnemies.setAll('body.velocity.y', -350);
        this.fishyEnemies.setAll('body.velocity.x', -200);
        this.fishyEnemies.callAll('sprite.add', 'dinoswim');
        this.fishyEnemies.callAll('animations.add', 'animations', 'dinoswim',[0,1,2,3], 8,true);
        this.fishyEnemies.callAll('play', null, 'dinoswim');
    },
    // creates flying enemies
    createFlyingEnemies: function() {
        this.flyingEnemies = this.game.add.group();
        this.flyingEnemies.enableBody = true;
        result = this.findObjectsByType('flyingEnemy', this.map, 'objects');
        result.forEach(function(element){
            this.createFromTiledObject(element, this.flyingEnemies);
        }, this);
        // sets enemy properties
        this.flyingEnemies.setAll('body.allowGravity', true);
        this.jumpingEnemies.setAll('body.immovable', true);
        this.flyingEnemies.setAll('anchor.x', 0.5);
        this.flyingEnemies.setAll('anchor.y', 0.5);
        this.flyingEnemies.setAll('body.velocity.x', -200);
        this.flyingEnemies.callAll('sprite.add', 'dinofly');
        this.flyingEnemies.callAll('animations.add', 'animations', 'dinofly',[0,1,2,3], 12, true);           
        this.flyingEnemies.callAll('play', null, 'dinofly');
    },
        // creates coins
        createCoins: function() {
            this.coins = this.game.add.group();
            this.coins.enableBody = true;
            result = this.findObjectsByType('coin', this.map, 'objects');
            result.forEach(function(element){
                this.createFromTiledObject(element, this.coins);
            }, this);
            // sets coin properties
            this.coins.setAll('anchor.x', 0.5);
            this.coins.setAll('anchor.y', 0.5);
            this.coins.callAll('sprite.add', 'coinflip');
            this.coins.callAll('animations.add', 'animations', 'coinflip', [0,1,2,3,4,5,6,7], 3, true);
            this.coins.callAll('play', null, 'coinflip');
        },

        // creates the goal
        createGoal: function() {
            result = this.findObjectsByType('goal', this.map, 'objects');
            this.goal = this.game.add.sprite(result[0].x, result[0].y, 'goal');
            this.goal.anchor.setTo(0.5, 0.5);
            this.game.physics.arcade.enable(this.goal);
        },


        //creates a sprite from an object
        createFromTiledObject: function(element, group) {
            var sprite = group.create(element.x, element.y, element.properties.sprite);

            //copies all properties to the sprite
            Object.keys(element.properties).forEach(function(key){
                sprite[key] = element.properties[key];
            });
        },

        // shoots a bullet
        shoot: function(){  
                // firerate 
                if (this.shotTimer < this.game.time.now){
                    this.shotTimer = this.game.time.now + 475;
                    // depending on player direction
                    if (this.direction == "right"){
                        // creates a bullet
                        var bullet2 =  this.bullets.create(this.player.body.x + this.player.body.width , this.player.body.y + this.player.body.height / 2 , 'bulletRight');
                            // sets bullet properties
                            this.game.physics.enable(bullet2, Phaser.Physics.ARCADE);
                            bullet2.outOfBoundsKill = true;
                            bullet2.anchor.setTo(0.5, 0.5);
                            bullet2.body.velocity.y = 0;
                            bullet2.body.velocity.x = 400;
                            bullet2.lifespan=500;
                            this.bulletArray.push(bullet2);
                            this.weaponSound.play();
                    }else{
                        var bullet2 =  this.bullets.create(this.player.body.x - this.player.body.width/2 + 15, this.player.body.y + this.player.body.height / 2 , 'bulletLeft');
                            this.game.physics.enable(bullet2, Phaser.Physics.ARCADE);
                            bullet2.outOfBoundsKill = true;
                            bullet2.anchor.setTo(0.5, 0.5);
                            bullet2.body.velocity.y = 0;
                            bullet2.body.velocity.x = -400;
                            bullet2.lifespan=500;
                            this.bulletArray.push(bullet2);
                            this.weaponSound.play();
                    } 
                }        
        },

        // renders the countdown timer
        render: function() {
            this.game.time.advancedTiming = true;
            if (this.timer.running) {
                    this.game.debug.text('Time: ' + this.formatTime(Math.round((this.timerEvent.delay - this.timer.ms) / 1000)), 10, 30, this.timeStyle.fill, this.timeStyle.font);
                }
            else {
                    this.game.debug.text("Over", 10, 30, this.timeStyle.fill, this.timeStyle.font);
            }
        },

        // pauses game if button is pressed
        pausePressed: function() {
            this.game.paused = true;
            this.pausedText.visible = true;
            this.input.onDown.add(function(){
                this.pausedText.visible = false;
                this.game.paused = false;
            }, this);
        },

            
        // formats the time of the counter
        formatTime: function(s) {
            var minutes = "0" + Math.floor(s / 60);
            var seconds = "0" + (s - minutes * 60);
            return minutes.substr(-2) + ":" + seconds.substr(-2);   
        },

        // collision method for bullet and blocked layer
        shootWall: function(bullet, blockedLayer){
            bullet.kill();
        },

        // collision method for bullet and enemy
        // bullet gets killed, scores and labels get updated, adds particle burst
        shootEnemy: function(bullet, enemy){
            bullet.kill();
            this.particleBurst(enemy);
            enemy.kill();
            this.killCount ++;
            this.killsLabel.text = 'Kills: ' + this.killCount;
            this.changeScore();
        },

        // collision method for player and enemy
        hitEnemy: function(player, enemy){
            this.playerDead();
    
        },

        // takes care of everythin if player dies
        playerDead: function(){
            this.deadSound.play();
            this.player.body.velocity.x = 0;
            this.player.kill();
            this.gameOver();
        },

        // checks the direction of the player
        checkDirection: function(direction){
            if (this.newDirection != this.direction){
                this.player.scale.x *= -1;
                this.direction = this.newDirection;
            }
        },

        // lets the enemy turn around if blocked
        enemyTurn: function(enemy, turningLayer){
            if (enemy.body.blocked.left){
                enemy.body.velocity.x = +50;
                enemy.scale.x *=-1; 
            } 
            if (enemy.body.blocked.right){
                enemy.body.velocity.x = -50;
                enemy.scale.x *=-1; 
            }
        },

        // lets the speedy enemy turn
        speedyEnemyTurn: function(enemy, turningLayer){  
            if (enemy.body.blocked.left){
                enemy.body.velocity.x = +250;
                enemy.scale.x *=-1; 
            } 
            if (enemy.body.blocked.right){
                enemy.body.velocity.x = -250;
                enemy.scale.x *=-1; 
            }
        },

        // lets the fishy enemy turn
        fishyEnemyTurn: function (enemy, turningLayer){
            if (enemy.body.blocked.left){
                enemy.body.velocity.x = +250;
                enemy.scale.x *=-1; 
            } 
            if (enemy.body.blocked.right){
                enemy.body.velocity.x = -250;
                enemy.scale.x *=-1; 
            }
            if (enemy.body.blocked.top){
                enemy.scale.y *=-1;
            }
            if (enemy.body.blocked.top){
                enemy.scale.y *=-1;
            }
        },

        // lets the flying enemy turn
        flyingEnemyTurn: function (enemy, turningLayer){
            if (enemy.body.blocked.left){
                enemy.body.velocity.x = +200;
                enemy.scale.x *=-1; 
            } 
            if (enemy.body.blocked.right){
                enemy.body.velocity.x = -200;
                enemy.scale.x *=-1; 
            }
        },

        // lets the enemy jump
        enemyJump: function(enemy, blockedLayer){
            if (enemy.body.blocked){
                    enemy.body.velocity.y = -350;
            }
        },

        // handles everything if a player collects a coin
        collectCoin: function(player, coin){
            coin.kill();
            this.coinsCollected ++;
            this.coinsLabel.text = 'Coins: ' + this.coinsCollected;
            this.changeScore();
            this.coinSound.play();
        },

        // sets position and strength of emitter
        particleBurst: function(enemy) {

            //  position the emitter where the enemy was
            this.emitter.x = enemy.x;
            this.emitter.y = enemy.y;
            this.emitter.start(true, 1500, null, 8);
        },

        // changes the score if needed
        changeScore: function(){
            this.score = this.killCount*5 + this.coinsCollected*3;
            this.scoreLabel.text = 'Score: ' + this.score;
        },

        // ends the game after the player died and starts the game over state
        gameOver: function(){
            this.music.stop();
            this.timer.stop();
            this.resetDirection();
            this.state.start('Over', true, false, this.level);

        },

        // ends the game after the player completed the level and starts the won screen
        levelComplete: function(){
            this.music.stop();
            this.finishTime = Math.round((this.timerEvent.delay - this.timer.ms) / 1000);
            this.timer.stop();
            this.resetDirection();
            this.state.start('Won', true, false, this.level, this.score, this.killCount, this.coinsCollected, this.finishTime);
        },

        // resets the direction
        resetDirection: function(){
            this.direction = "right";
            this.newDirection = "right";
        },



    update: function() {
        // checks if player fell to death
        if (this.player.bottom >= this.game.world.bounds.bottom){
            this.playerDead();
        }
        // bullet variable
        var singleBullet;

        // this lets player and blockedLayer collide
        this.game.physics.arcade.collide(this.player, this.blockedLayer);

        //this lets enemies and blockedLayer collide
        this.game.physics.arcade.collide(this.enemies, this.blockedLayer);
        this.game.physics.arcade.collide(this.speedyEnemies, this.blockedLayer);
        this.game.physics.arcade.collide(this.jumpingEnemies, this.blockedLayer, this.enemyJump, null, this);

        //this lets enemies and turningLayer collide
        this.game.physics.arcade.collide(this.enemies, this.turningLayer, this.enemyTurn, null, this);
        this.game.physics.arcade.collide(this.speedyEnemies, this.turningLayer, this.speedyEnemyTurn, null, this);
        this.game.physics.arcade.collide(this.fishyEnemies, this.turningLayer, this.fishyEnemyTurn, null, this);
        this.game.physics.arcade.collide(this.flyingEnemies, this.turningLayer, this.flyingEnemyTurn, null, this);

        // this lets player and enemies collide
        this.game.physics.arcade.collide(this.player, this.enemies, this.hitEnemy, null, this);
        this.game.physics.arcade.collide(this.player, this.jumpingEnemies, this.hitEnemy, null, this);
        this.game.physics.arcade.collide(this.player, this.flyingEnemies, this.hitEnemy, null, this);
        this.game.physics.arcade.collide(this.player, this.speedyEnemies, this.hitEnemy, null, this);
        this.game.physics.arcade.collide(this.player, this.fishyEnemies, this.hitEnemy, null, this);

        // this lets player and coins overlap
        this.game.physics.arcade.overlap(this.player, this.coins, this.collectCoin, null, this);

        // this lets player and goal overlap
        this.game.physics.arcade.overlap(this.player, this.goal, this.levelComplete, null, this);


        // this looks through all singleBullets in bulletArray and sets/checks collision
        for(var i=0; i < this.bulletArray.length; i++) {
            singleBullet = this.bulletArray[i];
            // this lets bullets collide with walls / blocking elements
            // if they collide, hitwall function is called
            this.game.physics.arcade.collide(singleBullet, this.blockedLayer, this.shootWall, null, this);
            // this lets bullets collide with enemies
            // if they collide, hitenemy function is called            
            this.game.physics.arcade.overlap(singleBullet, this.enemies, this.shootEnemy, null, this);
            this.game.physics.arcade.overlap(singleBullet, this.jumpingEnemies, this.shootEnemy, null, this);
            this.game.physics.arcade.overlap(singleBullet, this.flyingEnemies, this.shootEnemy, null, this);
            this.game.physics.arcade.overlap(singleBullet, this.speedyEnemies, this.shootEnemy, null, this);
            this.game.physics.arcade.overlap(singleBullet, this.fishyEnemies, this.shootEnemy, null, this);
        }

        // the body of the player does not move of its own
        this.player.body.velocity.x = 0;

        // this lets the player jump if the cursor up key is pressed
        if(this.cursors.up.isDown) {      
            this.playerJump();
        }

        // this lets the player move left if the cursor left key is pressed
        if(this.cursors.left.isDown) {
            this.player.animations.play('herowalk', 12, false);
            this.player.body.velocity.x -= 100;
            this.newDirection = "left";
        }

        // this lets the player move right if the cursor right key is pressed
        else if(this.cursors.right.isDown) {
            this.player.animations.play('herowalk', 12, false);
            this.player.body.velocity.x += 100;
            this.newDirection = "right";
        } else {
            this.player.body.velocity.x = 0;
            this.player.animations.stop();
            this.player.frame = 3;
        }

        // this lets the player shoot
        if (this.spacebar.isDown){
            this.shoot();
        }

        // this lets the player pause the game and open a menu
        if (this.esc.isDown){
            this.music.stop();
            this.state.start('Title');
        }

        // this checks the direction
        this.checkDirection(this.newDirection);
    },

};

