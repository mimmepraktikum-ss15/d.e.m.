// Title.js is the main menu screen of the game 

var dem = dem || {};

dem.Title = function(){};

dem.Title.prototype ={
	preload: function(){
	 
	},

	create: function(){
		// checks if menu music exists and/or is playing 
		// creates or starts music if needed
		if (!this.menuMusic||!this.menuMusic.isPlaying){	
			this.menuMusic = this.game.add.audio('music0', 0.5, true);
			this.menuMusic.play();
		}

		// logo
		this.logo = this.add.sprite(100, 10, 'logo');

		// big title graphics
		this.herotitle = this.add.sprite (21, 320-128-10, 'heroload');
		this.dinotitle= this.add.sprite (640-128-21, 320-128-10, 'dinoDead');

		// menu buttons
		this.playButton = this.game.add.button((640-64)/2, 236, 'play', this.playPressed, this);
		this.levelButton = this.game.add.button((640-64)/2-64-10, 278, 'level', this.levelPressed, this);
		this.scoreButton = this.game.add.button((640-64)/2, 278, 'score', this.scorePressed, this);
		this.helpButton = this.game.add.button((640-64)/2+64+10, 278, 'help', this.helpPressed, this);

		// emitter for bone particles 
		this.titleEmitter = this.game.add.emitter(0, 0, 100);
	    this.titleEmitter.makeParticles('bones');
	    this.titleEmitter.gravity = 200;
	    this.particlesGo();
	},

	// pressing play starts the game state and stops the menu music
	playPressed: function(){
		this.state.start ('Game');
		this.menuMusic.stop();
	},

	// pressing level starts the level selection state
	// the menuMusic is a parameter here because it may need to be stopped in the level selection
	levelPressed: function(){
		this.state.start ('Level', true, false, this.menuMusic);
	},

	// pressing score starts the score state
	scorePressed: function(){
		this.state.start ('Score');
	},

	// pressing help starts the help state
	helpPressed: function(){
		this.state.start ('Help');
	},

	// starts the particle emitter
	particlesGo: function(){
		// positioned on dino
        this.titleEmitter.x = this.dinotitle.x+64;
        this.titleEmitter.y = this.dinotitle.y+32;

        // The first parameter sets the effect to "explode" which means all particles are emitted at once
        // The second gives each particle a 2000ms lifespan
        // The third is ignored when using burst/explode mode
        // The final parameter (10) is how many particles will be emitted in this single burst
        this.titleEmitter.start(true, 10000,null, 80);
	},


}