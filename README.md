![Titelbild.png](https://bitbucket.org/repo/pzax9y/images/2632668597-Titelbild.png)

# D.E.M. - Dinosaur Eliminator Man #
Dinosaur Eliminator Man is a platformer built with Javascript, HTML and powered by the game engine Phaser (see [https://github.com/photonstorm/phaser](https://github.com/photonstorm/phaser)).

Inspiration comes from jump'n'run games like Mario or Mega Man.
This project is part of a university course.

## Setup ##
To play the game, simply click on this link: [https://dl.dropboxusercontent.com/u/116006676/source/index.html](https://dl.dropboxusercontent.com/u/116006676/D.E.M./index.html)

I used a Public Folder on a Dropbox Pro Account as an alternative to any web server, but it should work without problems on any hosting platform.

## Setting ##
In an ancient world filled with prehistoric dinosaurs the Dinosaur Eliminator Man (in short D.E.M.) has only one job - to survive!
Armed with only his gun, the D.E.M. fights his way through hordes of frightening enemies, jumps over hair-raising cliffs, collects coin after coin and explores different worlds.

## Features ##
* 2D gameworld
* 2D animated character
* 2D animated enemies (5 types)
* Sounds and music
* A small number of levels (3)
* A weapon to defend against the mean dinosaurs!
* Highscore mechanics (local)

## How to play ##
* Move left with left arrow key
* Move right with right arrow key
* Jump with with up arrow key
* Shoot weapon with space bar
* Quit with ESC key

## Tools used ##
* Coding: Sublime Text 2 ([https://www.sublimetext.com/](https://www.sublimetext.com/)
* Graphics and animation: Piskel [http://www.piskelapp.com/](http://www.piskelapp.com/)
* Graphics: Adobe Photoshop ([https://www.adobe.com/de/products/photoshop.html?promoid=KLXLS](https://www.adobe.com/de/products/photoshop.html?promoid=KLXLS))
* Sounds and music: Reaper [http://www.reaper.fm/](http://www.reaper.fm/)
* Map Editing: Tiled [http://www.mapeditor.org/download.html](http://www.mapeditor.org/download.html)
* Hosting: Dropbox [https://www.dropbox.com/](https://www.dropbox.com/)
* Game Engine: Phaser [http://phaser.io/](http://phaser.io/)

## Screenshots & Trailer ##
Trailer: [https://www.youtube.com/watch?v=TifsDtYwZjg](https://www.youtube.com/watch?v=TifsDtYwZjg)

Screenshots:
![Screenshot 1.png](https://bitbucket.org/repo/pzax9y/images/3281965205-Screenshot%201.png)
![Screenshot 2.jpg](https://bitbucket.org/repo/pzax9y/images/2948882087-Screenshot%202.jpg)
![Screenshot 3.jpg](https://bitbucket.org/repo/pzax9y/images/2965780324-Screenshot%203.jpg)
![Screenshot 4.jpg](https://bitbucket.org/repo/pzax9y/images/3301500742-Screenshot%204.jpg)
![Screenshot 5.jpg](https://bitbucket.org/repo/pzax9y/images/204888391-Screenshot%205.jpg)
![Screenshot 6.png](https://bitbucket.org/repo/pzax9y/images/2505346805-Screenshot%206.png)
![Screenshot 7.png](https://bitbucket.org/repo/pzax9y/images/1379979545-Screenshot%207.png)